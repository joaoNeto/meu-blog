<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'WO6p]-fzBlN:N{dv;fORAn2:K$2>b(s;GA7nEo 5SQ`k7=m?z^857VS1}+myZX2j');
define('SECURE_AUTH_KEY',  ':^#,yy4;,&?11P((Vi{/-,# oH@@W3]#AyU+pR6r8Xhorf>ixr5)TVzz;g$<sh!*');
define('LOGGED_IN_KEY',    '^,v= !Osb9#E8sdsLR9<*yKN[X.CVXE.zp%#W;c6Pn&3C.mXz-y:sstMI/cdrM{d');
define('NONCE_KEY',        ')4$,IdwE;u+bB^-wwp6^p~$an7jCFJ|X}@kFGZ0NC&nYP#~B]>?nA=OT~g{`y%0t');
define('AUTH_SALT',        '7N5dBExJp|:)3O?v7/IDTRH 7p[:s9Pwlw8QAMkAV*C2|~=-;UdsfH[dO>BH-):@');
define('SECURE_AUTH_SALT', ';;(V)mI5%a+<e L;2g&Q8<3WH@1@bgy;#x.F4Nb@m4;^3DhzY%.Q0RG5{A{H141v');
define('LOGGED_IN_SALT',   'A;(G~iFVR=K)/NK|Qh-G}#Ll8r#a9Qoh+*H_F;mi)-r;Bl8>Otp.oELc,ay6II@k');
define('NONCE_SALT',       'pe,c/Me@#~l_}uM2kj!m7M?cpj>hU[qR@mpGE.c.%Ro{O?W<{m2LW9yc-QX65p|G');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'i_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
